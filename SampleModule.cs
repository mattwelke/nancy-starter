using Nancy;

namespace NancyTest
{
    public class SampleModule : NancyModule
    {
        public SampleModule()
        {
            Get("/", _ => "Hello World!");
        }
    }
}
